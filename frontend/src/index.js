import React from 'react';
import ReactDOM from 'react-dom';
import Homepage from 'homepage/Homepage';
import Listing from 'listing/Listing';
import RoomList from 'roomlist/RoomList';
import RoomDetail from 'roomdetail/RoomDetail';
// var Listing = require('listing/components/listing');

window.app = {

    showHomePage: function(id,data){
        ReactDOM.render(
          <Homepage data={data} />, document.getElementById(id)
        );
    },

    showListingSpaceForm:function(id){
      ReactDOM.render(
          <Listing />, document.getElementById(id)
        );
    },

      showRoomDetail:function(id,data){
      ReactDOM.render(
          <RoomDetail data={data} />, document.getElementById(id)
        );
    },

    showRoomList:function(id){
      ReactDOM.render(
          <RoomList />,document.getElementById(id)
        );
    }
}
import React, {Component} from 'react';

export default class RoomDetail extends Component{
    constructor(props){
        super(props);
        this.state = { rooms:[] }
    }
    componentDidMount(){
        console.log('componentDidMount');
        console.log(this.props.data.slug);
        this.loadRoomFromServer();
    }
    loadRoomFromServer(){
        $.ajax({
            url:'/api/v1/rental/'+this.props.data.slug,
            dataType:'json',
            success: (data) => {
                console.log('data',data);
                this.setState({rooms: data});
              },
              error: (xhr, status, err) => {
                console.error(url, status, err.toString());
              }
            });
    }
    render() {
        console.log(this.state.rooms.amenities);
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-12">
                        <img src="" />
                    </div>
                </div>
                <div className="col-md-6">
                    <ul className="list-group">
                        <li>Owner Name</li>
                        <li>Email</li>
                        <li>Phone Number</li>
                        <li>Listing Name</li>
                        <li>Summary on Listing</li>
                        <li>Property Type</li>
                        <li>No of Rooms</li>
                        <li>Price of Rent</li>
                        <li>City</li>
                        <li>Place</li>
                        <li>Water Facility</li>
                        <li>Amenities</li>
                    </ul>
                </div>
                <div className="col-md-6">
                    <ul className="list-group">
                        <li>{this.state.rooms.ownerName}</li>
                        <li>{this.state.rooms.email}</li>
                        <li>{this.state.rooms.phoneNumber}</li>
                        <li>{this.state.rooms.listingName}</li>
                        <li>{this.state.rooms.summary}</li>
                        <li>{this.state.rooms.property}</li>
                        <li>{this.state.rooms.room}</li>
                        <li>{this.state.rooms.price}</li>
                        <li>{this.state.rooms.city}</li>
                        <li>{this.state.rooms.place}</li>
                        <li>{this.state.rooms.water}</li>
                        <li>{this.state.rooms.amenities}</li>
                    </ul>
                </div>
            </div>
            );
    }
}
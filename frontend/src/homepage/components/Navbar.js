import React from 'react';

export default class Navbar extends React.Component {
  render() {
    console.log(this.props.data);
    return (
        <nav className="navbar navbar-default">
          <div className="container-fluid">
                <div className="navbar-header">
                  <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                  </button>
                  <a href="/" className="navbar-brand"><span>Rental Space</span></a>
                </div>

                 <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul className="nav navbar-nav navbar-right">
                        <li><a href="/">Home</a></li>
                        <li><a href="/listing/space/">List Space</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    );
  }
}

import React from 'react';

export default class Body extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
        var margin = { marginTop : '8em' };
        return(
            <div className = "container">
                <div className="content text-align-center" style={margin} >
                    <div className="row text-center">
                        <div className="middle-text">
                            <h1><span>Rental Space Welcome's you </span></h1>
                            <button className="btn how-it-works">Search Space</button>
                        </div>
                    </div>
                </div>
            </div>

            );
    }
}

import React from 'react';
import AddRent from './components/AddRent';
import Navbar from './components/Navbar';
import Body from './components/Body';

class Listing extends React.Component{
   constructor(props,context) {
          super(props,context);
      }
   render(){
    return(
        <div className="nav">
          <Navbar />
          <Body />
          <AddRent />
        </div>
      )
   }
}

export default Listing;
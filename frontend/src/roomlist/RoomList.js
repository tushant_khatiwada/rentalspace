import React from 'react';
import ReactDOM from 'react-dom';


export default class RoomList extends React.Component{
    constructor(props){
        super(props);
        this.state = { rooms: [] }
    }

    componentDidMount(){
        console.log('componentDidMount');
        this.loadRoomFromServer();
    }

    loadRoomFromServer(){
        $.ajax({
            url:'/api/v1/rental/',
            dataType:'json',
            success: (data) => {
                console.log('data',data);
                this.setState({rooms: data.objects});
                console.log('success');
              },
              error: (xhr, status, err) => {
                console.error(url, status, err.toString());
              }
            });
    }

    render(){
        console.log('rooms',this.state.rooms);
        let listOfRoom = this.state.rooms.map((room,id)=>{
            return(
                    <Rooms key={id} 
                    name={room.listingName} 
                    price={room.price} 
                    number={room.room} 
                    gallery={room.gallery} />
                );
        });
        console.log('listOfRoom',listOfRoom);

        return(
                <div className="container-fluid">
                    <div className="row">
                            { listOfRoom }
                    </div>
                </div>
            )
    }
}

class Rooms extends React.Component{
    render(){
        console.dir('directory of gallery',this.props.gallery);
        let imageFile = this.props.gallery.map((image) => {
            return(
                    <img src={image.image} className="img-responsive" />
                );
        });

        return(
                <div className="col-md-4">
                    { imageFile[0] }
                    <h2>{this.props.name}</h2>
                    <p>{this.props.price}</p>
                    <p>{this.props.number}</p>
                </div>
            )
    }
}

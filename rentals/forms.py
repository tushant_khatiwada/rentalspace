from django import forms
from .models import Rental

import logging 
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class SearchForm(forms.Form):
    landmark=forms.CharField(max_length=100)
    
    

# class RentalForm(forms.ModelForm):
#     files = MultiFileField(min_num=1, max_num=3, max_file_size=1024*1024*5)

#     def __init__(self, *args, **kwargs):
#         super(RentalForm, self).__init__(*args, **kwargs)
#         self.fields['title'].required = True
#         self.fields['location'].required = True
#         self.fields['bedroom_count'].required = True
#         self.fields['has_kitchen'].required = False
#         self.fields['rent_price'].required = True
#         self.fields['other_description'].required = False
#         self.fields['phone'].required = True
#         self.fields['owner_full_name'].required = False


#     class Meta:
#         model = Rental
#         fields = ['title', 'location', 'bedroom_count', 'has_kitchen',
#                     'rent_price','other_description','phone','owner_full_name']


#     def save(self, *args, **kwargs):
#         instance = super(RentalForm, self).save(*args, **kwargs)
#         instance.save_images(self.cleaned_data["files"],instance)
#         return instance 

# class LogForm(forms.Form):
#     _type = forms.CharField(max_length=10)
#     _id = forms.IntegerField()





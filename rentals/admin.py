from django.contrib import admin
from rentals.models import Rental,GalleryImage

# Register your models here.
# class RentalAdmin(admin.ModelAdmin):
#     # prepopulated_fields = {'slug':['place']}
#     class Meta:
#         model = Rental
# class GalleryAdmin(admin.ModelAdmin):
# 	list_display = ('image',)

# admin.site.register(Rental,RentalAdmin)
# # admin.site.register(Log)
# admin.site.register(Gallery,GalleryAdmin)

# class InlineGalleryImage(admin.TabularInline):
#     model = Rental
#     extra = 2

# class RentalAdmin(admin.ModelAdmin):
#     # model = Rental
#     # filter_horizontal=('image',)
#     inlines = [InlineGalleryImage,]

# admin.site.register(Rental, RentalAdmin)
# admin.site.register(GalleryImage)


class InlineGalleryImage(admin.TabularInline):
    model = GalleryImage
    fk_name = 'rental'

class RentalAdmin(admin.ModelAdmin):
    inlines = [
        InlineGalleryImage,
    ]
admin.site.register(Rental,RentalAdmin)
admin.site.register(GalleryImage)



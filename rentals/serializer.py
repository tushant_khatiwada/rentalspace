from rest_framework import serializers 
from .models import *


class GallerySerializer(serializers.ModelSerializer):
    class Meta:
        model = GalleryImage
        fields=('pk','image') 
        

class RentalSerializer(serializers.ModelSerializer):
    image = GallerySerializer(many=True)
    class Meta:
        model = Rental
        fields = ('pk','listingName','property','city','place','ownerName','room','water','amenities','price','summary','phoneNumber','email','image')

    def create(self,validated_data):
        listingName=validated_data.get('listingName',None)
        property=validated_data.get('property',None)
        city=validated_data.get('city',None)
        place=validated_data.get('place',None)
        ownerName=validated_data.get('ownerName',None)
        room=validated_data.get('room',None)
        water=validated_data.get('water',None)
        amenities=validated_data.get('amenities',None)
        price=validated_data.get('price',None)
        summary=validated_data.get('summary',None)
        phoneNumber=validated_data.get('phoneNumber',None)
        email=validated_data.get('email',None)
        image=validated_data.pop('image')
        return Rental.objects.create(listingName=listingName,property=property,city=city,place=place,ownerName=ownerName,
            room=room,water=water,amenities=amenities,price=price,summary=summary,phoneNumber=phoneNumber,email=email,image=image)




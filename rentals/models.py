from django.db import models
from django.utils.translation import ugettext_lazy as _
# from multiselectfield import MultiSelectField
#from django.core.files.storage import FileSystemStorage
from django.utils.text import slugify
from django.db.models.signals import pre_save
#from stdimage.models import StdImageField
import base64 
from PIL import Image, ImageDraw, ImageFont
from io import BytesIO
from django.conf import settings




class Rental(models.Model):
    """
    Rents
    """
    PROPERTY_CHOICES=(
        ('A','Appartment'),
        ('H','House'),
        ('S','Shop'),
        ('B','Bunglow'),
        ('H','Hostel'),
        )
    AMENITIES_CHOICES=(
        ('k','Kitchen'),
        ('A','Attach Bathroom'),
        ('I','Internet'),
        ('C','Cable'),
        )
    WATER_CHOICES=(
        ('Y','Yes'),
        ('N','No'),
        ('T','Takes Charge'),
        )
    ownerName = models.CharField(_("Owner's Name"),max_length=255, blank=True,null=True,
        help_text=_("Owner's Full Name"))
    email = models.CharField(max_length=120,blank=True,null=True)
    phoneNumber = models.PositiveIntegerField(blank=False,null=True,
        help_text=_("Phone number of contact person"))
    listingName =  models.CharField(_("Lisitng Name"), max_length=255, blank=False,null=True,
        help_text=_("Title of the rental space"))
    slug = models.SlugField(unique=True,blank=False,null=True)
    summary = models.TextField(max_length=500, blank=True,null=True,help_text=_("Description of the rental space"))
    property = models.CharField(_("Property type"),max_length=10,null=True)
    room = models.PositiveIntegerField(_("No of Rooms"), blank=False, null=True,
        help_text=_("Number of bedrooms available"))
    price = models.PositiveIntegerField(blank=False,null=True,
        help_text=_("Rental price of the space per month"))
    city =  models.CharField(_("City"), max_length=255, blank=False,null=True,
        help_text=_("City of the rental space"))
    place =  models.CharField(_("Place"), max_length=255, blank=False,null=True,
        help_text=_("Place of the rental space"))
    water = models.CharField(_("water facilities"),max_length=50,null=True,
        help_text=_("Is there water facility?"))
    amenities = models.CharField(_("amenities"),max_length=100,blank=True,null=True)
    phone_image = models.CharField(max_length=2048,blank=True,null=True,
        help_text=_("image form of the phone number"))
    is_published = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if self.phoneNumber:
            print(self.phoneNumber)
            font = ImageFont.truetype(settings.PHONE_FONT,14)
            phone_image=Image.new("RGBA", (120,16),(255, 255, 255))
            draw = ImageDraw.Draw(phone_image) 
            draw.text((0, 0), self.phoneNumber, (0,0,0), font=font)

            byte_stream = BytesIO()
            phone_image.save(byte_stream, format="png")
            byte_stream.seek(0)

            self.phone_image = base64.b64encode(byte_stream.read()).decode()
        return super(Rental,self).save( *args, **kwargs)


    def __str__(self):
        return self.listingName

    class Meta:
        verbose_name = _("Rent")
        verbose_name_plural = _("Rents")

def pre_save_post_receiver(sender,instance,*args,**kwargs):
    slug = slugify(instance.listingName)
    exists = Rental.objects.filter(slug=slug).exists()
    if exists:
        slug = "%s-%s"%(slug,instance.id)
    instance.slug = slug

pre_save.connect(pre_save_post_receiver,sender=Rental)


class GalleryImage(models.Model):
    rental = models.ForeignKey('Rental',on_delete=models.CASCADE,blank=True,null=True,
                                verbose_name=_('Rental'), related_name="gallery")
    image = models.ImageField(blank=True,upload_to='upload/',null=True)
   

    class Meta:
        verbose_name = _('Gallery')
        verbose_name_plural = _('Galleries')


    def __str__(self):
        image_name = self.image.name.find('/')
        return self.image.name[image_name+1:]
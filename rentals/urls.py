
from django.conf.urls import include, url
from django.contrib import admin
from .models import Rental
from rentals.api.api import RentalResource,UserResource,GalleryImageResource
from rest_framework.urlpatterns import format_suffix_patterns
from tastypie.api import Api
from .views import (    LandingView, SearchView, Language, 
                        AddView, RoomList, UploadImage,
                        FilterSpace,AddSpaceView,rent_detail,
                        ImageUpload
                  )

v1_api = Api(api_name='v1')
v1_api.register(UserResource())
v1_api.register(RentalResource())
v1_api.register(GalleryImageResource())
user_resource = UserResource()
rental_resource = RentalResource()

urlpatterns = [
    url(r'^$', LandingView.as_view(), name="landing_page"),
    url(r'^add/$', AddView.as_view(), name="add"),
    url(r'^roomlist/$', RoomList.as_view(), name="roomlist"),
    url(r'^rent/(?P<slug>\w+)/$', rent_detail, name="rent_detail"),
    url(r'^add/space/$', AddSpaceView.as_view(), name="addSpace"),
    url(r'^lang/$', Language.as_view(), name="lang"),
    url(r'^search/$', SearchView.as_view(), name="search"),
    url(r'^upload/image/$', UploadImage.as_view(), name="uploadImage"),
    url(r'^upload/image/(?P<pk>\d+)/$', ImageUpload, name="ImageUpload"),
    url(r'^filter/space/$', FilterSpace.as_view(), name="filterSpace"),
    url(r'^api/', include(v1_api.urls)),
]

urlpatterns = format_suffix_patterns(urlpatterns)

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = urlpatterns + \
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

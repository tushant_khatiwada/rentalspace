import os
from django.conf import settings
from django import template
from PIL import Image

register = template.Library()

@register.inclusion_tag("includes/featured_image.html",takes_context=True)
def featured_image_for(context, rental, width=0, height=0):
    """
    provides banner image for rental,
    """
    image = None
    try:
        image = rental.gallery.all()[0]
    except:
        return context

    resized_path = resize_and_crop(image.file.name, (width, height,))
    context["feature_image_url"] =os.path.join(settings.MEDIA_URL, resized_path)

    return context



def resize_and_crop(img_path, size, crop_type='middle'):
    """
    Function source: https://gist.github.com/sigilioso/2957026#comment-1241684
    ** modified **

    Resize and crop an image to fit the specified size.

    args:
    img_path: path for the image to resize. This should be an absolute path from MEDIA_ROOT.
    size: `(width, height)` tuple.
    crop_type: can be 'top', 'middle' or 'bottom', depending on this
    value, the image will cropped getting the 'top/left', 'middle' or
    'bottom/right' of the image to fit the size.
    raises:
    Exception: if can not open the file in img_path of there is problems
    to save the image.
    ValueError: if an invalid `crop_type` is provided.
    """
    if size[0] == 0 and size[1] == 0:
        return img_path

    output_file = '.'.join(img_path.split('.')[:-1])
    output_file = "{}_{}_{}.jpg".format(output_file, size[0], size[1])
    output_full_path = os.path.join(settings.MEDIA_ROOT, output_file)
    img_path = os.path.join(settings.MEDIA_ROOT, img_path)

    # If height is higher we resize vertically, if not we resize horizontally
    img = Image.open(img_path)

    if size[0] == 0:
        size = (img.size[0], size[1])
    if size[1] == 0:
        size = (size[0], img.size[1])

    # Get current and desired ratio for the images
    img_ratio = img.size[0] / float(img.size[1])
    ratio = size[0] / float(size[1])
    #The image is scaled/cropped vertically or horizontally depending on the ratio
    if ratio > img_ratio:
        img = img.resize((size[0], int(round(size[0] * img.size[1] / img.size[0]))),
            Image.ANTIALIAS)
        # Crop in the top, middle or bottom
        if crop_type == 'top':
            box = (0, 0, img.size[0], size[1])
        elif crop_type == 'middle':
            box = (0, int(round((img.size[1] - size[1]) / 2)), img.size[0],
                int(round((img.size[1] + size[1]) / 2)))
        elif crop_type == 'bottom':
            box = (0, img.size[1] - size[1], img.size[0], img.size[1])
        else :
            raise ValueError('ERROR: invalid value for crop_type')
        img = img.crop(box)
    elif ratio < img_ratio:
        img = img.resize((int(round(size[1] * img.size[0] / img.size[1])), size[1]),
            Image.ANTIALIAS)
        # Crop in the top, middle or bottom
        if crop_type == 'top':
            box = (0, 0, size[0], img.size[1])
        elif crop_type == 'middle':
            box = (int(round((img.size[0] - size[0]) / 2)), 0,
                int(round((img.size[0] + size[0]) / 2)), img.size[1])
        elif crop_type == 'bottom':
            box = (img.size[0] - size[0], 0, img.size[0], img.size[1])
        else :
            raise ValueError('ERROR: invalid value for crop_type')
        img = img.crop(box)
    else :
        img = img.resize((size[0], size[1]),
            Image.ANTIALIAS)
    # If the scale is the same, we do not need to crop
    img.save(output_full_path)
    return output_file


@register.inclusion_tag("includes/images_gallery.html",takes_context=True)
def gallery_for(context, rental, width=0, height=0):
    """
    provides banner image for rental,
    """
    context["images"]=[]
    for image in rental.gallery.all():

        thumb_path = resize_and_crop(image.file.name, (width, height,))
        thumb_full_path=os.path.join(settings.MEDIA_URL, thumb_path)

        context["images"].append({
            "original": image.file.url,
            "thumbnail": thumb_full_path,
            })

    return context

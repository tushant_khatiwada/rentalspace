import json
from django.db.models import Count
from django.db.models import Q
from django.core import serializers

from django.core.urlresolvers import reverse
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpRequest 
from django.utils.translation import ugettext_lazy as _
from django.views.generic import View, TemplateView, DetailView

# from rentals.models import Rental
import base64 
from PIL import Image, ImageDraw, ImageFont
from io import BytesIO
                

from django.conf import settings 

from .forms import SearchForm
from .models import Rental,GalleryImage

#import logging
# logger = logging.getLogger(__name__) 
# logger.setLevel(logging.DEBUG)

class Language(View):
    def dispatch(self, request, *args, **kwargs):
        from django.utils import translation
        lang=None
        if "language" in request.GET:
            lang = request.GET.get("language", "en-us")
            logger.debug(lang)
            translation.activate(lang)
            if not lang in ("en-us", "ne"):
                lang="en-us"
        referer = request.META.get("HTTP_REFERER")
        if referer is None:
            referer = '/' 
        response = HttpResponseRedirect(referer)

        if not lang is None:
            response.set_cookie(settings.LANGUAGE_COOKIE_NAME, lang)
        return response

class LandingView(TemplateView):
    template_name="landing.html"
    
class RentDetailView(DetailView):
    template_name="rentals/detail.html"
    model = Rental


class UploadImage(View):
    model = Rental
    def post(self,request,*args,**kwargs):
        print(request)
        f =request.FILES.getlist('image')
        print ('f',f)
        if request.FILES:
            # files = [request.FILES.getlist('image[%d]'%i) for i in range(0,len(request.FILES))]
            # rentalInstance = Rental.objects.all()
            # rentalid = Rental.objects.get(id)
            # print ('rental id is', rentalid)
            # rental = Rental.objects.get(id=rentalid)
            for file in request.FILES.getlist('image'):
                print('file',file)
                image = GalleryImage.objects.create(image=file)
                print('image',image)
                image.save()
        return HttpResponseRedirect('/')

def ImageUpload(request,pk=None):
    if request.POST or request.FILES:
        rental = Rental.objects.get(id=pk)
        for file in request.FILES.getlist('image'):
            image = GalleryImage.objects.create(image=file,rental=rental)
            image.save()
        return render(request,'rentals/add.html')

class AddSpaceView(View):
    def post(self,request,*args,**kwargs):
        print ('add space view',request)
        if request.POST:
            print('owner name is',request.POST.get('ownerName'))
            print('amenities',request.POST.get('amenities'))
            rental = Rental()
            rental.ownerName = request.POST.get('ownerName')
            rental.email = request.POST.get('email')
            rental.phoneNumber = request.POST.get('phoneNumber')
            rental.listingName = request.POST.get('ownerName')
            rental.summary = request.POST.get('summary')
            rental.property = request.POST.get('property')
            rental.room = request.POST.get('room')
            rental.price = request.POST.get('price')
            rental.city = request.POST.get('city')
            rental.place = request.POST.get('place')
            rental.water = request.POST.get('water')
            rental.amenities = request.POST.get('amenities')
            rental.save()
        return HttpResponseRedirect('/')

class FilterSpace(View):
    def get(self,request,*args,**kwargs):
        result = dict()
        data_list = []
        result['status'] = "success"
        property = request.GET.get('property',None)
        room = request.GET.get('room', None)
        price = request.GET.get('price', None)
        rental = Rental.objects.all()
        if room:
            rental = rental.filter(room=room)
            data_list.append(rental)
            print('datalist',data_list)
        if price:
            rental = rental.filter(price__lte=price)
            data_list.append(rental)
            print('datalist',data_list)
        if property:
            rental = rental.filter(property=property)
            data_list.append(rental)
            print('datalist',data_list)
        result['data']=data_list
        rental_json =  serializers.serialize('json',result)    
        return HttpResponse(json.dumps(rental_json), content_type='application/x-json')        
       

class SearchView(TemplateView):
    template_name = "rentals/search.html"


    def get(self,request,*args,**kwargs):
        context={}
        form = SearchForm(request.GET)
        if form.is_valid():
            search_landmark = form.cleaned_data["landmark"]
            # context['rentals'] = Rental.objects.all()
            search_result = Rental.objects.filter(Q(place__contains = search_landmark)|Q(place__startswith=search_landmark))
            if search_result:
                context['search_landmark'] = search_landmark
                context['search_result'] = search_result
                return render(request,self.template_name,context)
            else:
                messages.error(request, "{} '{}'".format(_("Couldn't find any rental at"), search_landmark))
            return HttpResponseRedirect('/')
        else:
            messages.error(request, _("Invalid search query."))
            return HttpResponseRedirect('/')



class AddView(TemplateView):
    template_name = 'rentals/add.html'

    def get_context_data(self, **kwargs):
        context = super(AddView, self).get_context_data(**kwargs)
        context['object_list'] = Rental.objects.all()
        print('context', context)
        return context

class RoomList(TemplateView):
    template_name = 'rentals/list.html'

def rent_detail(request, slug=None):
    instance = get_object_or_404(Rental, slug=slug)
    print('instance',instance)
    context = {
        'slug':instance.slug,
        'instance':instance
    }
    return render(request,'rentals/rent_detail.html',context)
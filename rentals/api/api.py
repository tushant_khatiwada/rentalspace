from tastypie.resources import ModelResource
from tastypie.constants import ALL
from tastypie import fields
from tastypie.authorization import DjangoAuthorization
from tastypie.authentication import BasicAuthentication
from tastypie.serializers import Serializer
from tastypie.throttle import BaseThrottle
from rentals.models import Rental,GalleryImage
from django.contrib.auth.models import User

import logging
logr = logging.getLogger(__name__)
class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        excludes = ['email','password','is_active','is_staff','is_superuser']
        allowed_method = ['get']
        throttle = BaseThrottle(throttle_at=100)
        serializer = Serializer(formats=['json', 'jsonp', 'xml'])

class MultipartResource(object):
    def deserialize(self, request, data, format=None):
        if not format:
            format = request.META.get('CONTENT_TYPE', 'application/json')

        if format == 'application/x-www-form-urlencoded':
            return request.POST

        if format.startswith('multipart'):
            print('tushant')
            data = request.POST.copy()
            data.update(request.FILES)
            print('data is',data)
            return data
        return super(MultipartResource, self).deserialize(request, data, format)


    def put_detail(self, request, **kwargs):
        if request.META.get('CONTENT_TYPE').startswith('multipart') and \
                not hasattr(request, '_body'):
            request._body = ''
        return super(MultipartResource,self).put_detail(request,**kwargs)

    def patch_detail(self, request, **kwargs):
        if request.META.get('CONTENT_TYPE', '').startswith('multipart/form-data') and not hasattr(request, '_body'):
             request._body = ''
        return super(MultipartResource, self).patch_detail(request, **kwargs)


class RentalResource(MultipartResource,ModelResource):
    gallery = fields.ToManyField('rentals.api.api.GalleryImageResource', 'gallery', related_name='rental',full=True)
    class Meta:
        queryset = Rental.objects.all()
        resource_name = 'rental'
        allowed_methods = ['get', 'post','put']
        fields = ['listingName','slug','property','city','place','ownerName','room','water','amenities','price','summary','phoneNumber','email']
        filtering = { "property" : ALL , "room":ALL,"price":ALL,"listingName":ALL,"slug":ALL}
        authorization = DjangoAuthorization()


class GalleryImageResource(ModelResource):
    rental = fields.ForeignKey(RentalResource, 'rental',full=False, null=True, blank=True)
    class Meta:
        queryset = GalleryImage.objects.all()
        resource_name = 'gallery'
        allowed_methods = ['get','post','put']
        authorization = DjangoAuthorization()
var gulp = require('gulp');
var minifyCSS = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var reactify = require('reactify'); 
var babelify = require('babelify');
var watchify = require('watchify');

var cssSrcDir = './css/';
var jsSrcDir = './js/react/';
var buildDir = './build/';
var distDir = './dist/';
var mapsDir = './maps/';


gulp.task('minify-css',function(){
    return gulp.src(cssSrcDir + '**/*.css')
    .pipe(minifyCSS())
    .pipe(gulp.dest(buildDir+'/css'))
});

gulp.task('uglify',function(){
    return gulp.src(jsSrcDir + '/**/*.js')
    .pipe(uglify())
    .pipe(gulp.dest(buildDir+'/js/'))
});

gulp.task('js', function (done) {
  [
    "homepage",
    "Filter",
    "listing",
  ].forEach(function (entry, i, entries) {
    // Count remaining bundling operations to track
    // when to call done(). Could alternatively use
    // merge-stream and return its output.
    entries.remaining = entries.remaining || entries.length;

   browserify('./js/react/' + entry + '.jsx').transform(babelify, {presets: ['es2015', 'react']})
      .bundle()
      // If you need to use gulp plugins after bundling then you can
      // pipe to vinyl-source-stream then gulp.dest() here instead
      .pipe(
        require('fs').createWriteStream(buildDir + entry + 'Bundle.js')
        .on('finish', function () {
          if (! --entries.remaining) done();
        })
      );
  });
});


gulp.task('minify',['minify-css','uglify','js']);




// var gulp = require('gulp');
// var gutil = require('gulp-util');
// var source = require('vinyl-source-stream');
// var browserify = require('browserify');
// var reactify = require('reactify');
// var watchify = require('watchify');
// var factor = require('factor-bundle');
// var uglify = require('gulp-uglify');
// var fs = require('fs');
// var concat = require('concat-stream');
// var file = require('gulp-file');

// gulp.task('watch', bundle)

// function bundle () {

//   // react components
//   var files = [
//     '/path/to/file1.jsx',
//     '/path/to/file2.jsx',
//     '/path/to/file3.jsx'
//   ];


//   var bundler = watchify(browserify(watchify.args)) 

//   bundler.add(files);
//   bundler.add('./lib/api.js', {expose: 'api'});
//   bundler.require('./lib/api.js', {expose: 'api'});
//   bundler.transform('reactify');
//   bundler.on('update', rebundle);

//   function rebundle() {
//     bundler.plugin('factor-bundle', {
//         outputs: [
//           write('/path/to/file1.js'),
//           write('/path/to/file2.js'),
//           write('/path/to/file3.js'),
//           ]
//     });
//     bundler.bundle()
//         .on('error', gutil.log.bind(gutil, 'Browserify Error'))
//         .pipe(write('shared.js'));
//   };

//   return rebundle();
// }


// function write (name) {
//     return concat(function (content) {
//         // create new vinyl file from content and use the basename of the
//         // filepath in scope as its basename.
//         return file(name, content, { src: true })
//         // uglify content
//         .pipe(uglify())
//         // write content to build directory
//         .pipe(gulp.dest('./public/bundles/'))
//     });
// }